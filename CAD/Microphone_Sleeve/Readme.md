# Microphone Sleeve #

The sleeve will fit the VideoTeam's Shure SM58 wireless mics, it screws on
instead of the original battery cover.

The optional light can be "twisted on" the bottom of the sleeve. It's cavity is
made to hold a CR2032 battery holder, a LED and some circuitry so that the
audio master will be able to figure out which mic is which.

![Microphone with sleeve and light](mic_with_sleeve_sm.jpg)

([large image](mic_with_sleeve.jpg))


## Printing

For the sleeve, note that you need to combine the thread and the main part of
the sleeve when you export the STL -- Just select both when exporting it.
(Making a fusion seems not to work in this version of FreeCAD; if you do you'll
only get the thread...)

I suggest to print the parts in PETG, as the thread will work well with it (it
has lower friction). I've used two perimeters and dynamic layer heights, as the
thread area needs a nice resolution (e.g 0.15mm)

For the light, I've used transparent PLA with the extrusion multiplier cranked up
a bit (5%) and a default extrusion width set to 250% to optimize transparency a bit.

The lights have three holes to cut a M3 thread into them and add a M3x8
countersunk screw for the bayonet twist on mechanism.

## LED Circuit

The circuit is a simple current source. Although schematic is not very accurate but
will limit the current to +-5mA, largely independent whether the battery is full
or half empty and this will extend battery life.

The circuit fits on a small breadboard PCB on top of the battery holder.

```
  BAT+ ------------+---------------------+
                   |                     |
                   |                     |
	           .-.                    V  LED
                  | | R1                 -
                  | |                    |
                  '-'                    |
                   |                     |
                   |                     |
                N1 +----------+          |
                   |          |          |
                   |          |          |
                   V D1       |          |
                   -          |          |
	            |         .-.         |
                   V D2      | | R2      |
                   -         | |         |
                   |         '-'         |
                   V D3       |        |/
                   -          +--------|  T1
                   |                   |>
                   |                     |
                   |                     |
  BAT- ------------+----------------------

  R1 ~ 19k
  R2 = 38k
  D1…D3 = 1N4148 (any Si-diode will do it; could be replaced by a zener diode; I didn't have one)
  T1 = BC846B (any NPN will do it, this one has a dc-gain of ~250)
  LED = OSRAM Lx W5KM 
```

![Light circuit on bread board](light-circuit_sm.jpg)

([large image](light-circuit.jpg))

### back-of-the-envelope-calculation:

D1...D3 sets the voltage at N1 to ~1.4V, so that R2 will roughly have a voltage
drop of 1.4-0.65 = 0.75V, supplying T1 with a base current of 0.75V/38k = 20µA,
which yields a LED current of around 250*20µA = 5mA.


(License for all files in this folder: CC BY-SA 4.0)

