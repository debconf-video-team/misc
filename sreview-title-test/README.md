## Preview SReview titles

1. Scrape the conference schedule, with:
   `./scrape-schedule.py https://example.com/schedule/pentabarf.xml`
   or write some test data by hand into `talks.yml`.
2. Run `./preview.py --template opening.svg`.
   Look at `--help` for more details.
3. Review the output: `sensible-browser output/index.html`.
