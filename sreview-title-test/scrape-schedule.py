#!/usr/bin/env python3

from argparse import ArgumentParser
from dataclasses import asdict
from xml.etree import ElementTree

import requests
from yaml import dump

from preview import Talk


def main():
    p = ArgumentParser()
    p.add_argument('pentabarf_url', help='Pentabarf XML Schedule URL')
    args = p.parse_args()

    with requests.get(args.pentabarf_url) as r:
        tree = ElementTree.fromstring(r.content)

    talks = []
    for room in tree.findall('.//room'):
        for event in room.findall('event'):
            speakers = [person.text
                        for person in event.findall('persons/person')]
            talks.append(Talk(
                speakers=speakers,
                room=room.attrib['name'],
                title=event.findtext('title'),
                subtitle='',
                date=event.findtext('date').split('T')[0],
                apology='This is a test apology. '
                        'Everything went horribly wrong. Sorry about that'))

    with open('talks.yml', 'w') as f:
        dump([asdict(talk) for talk in talks],
             f, explicit_start=True, default_flow_style=False)


if __name__ == '__main__':
    main()
