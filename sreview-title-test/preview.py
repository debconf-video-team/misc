#!/usr/bin/env python3

from argparse import ArgumentParser
from dataclasses import dataclass
from pathlib import Path
from typing import List
from shutil import rmtree
from subprocess import Popen, PIPE
from xml.sax.saxutils import escape as xml_escape

from yaml import safe_load


HEAD = """\
<!doctype html>
<html>
<head>
  <title>Title Previews</title>
</head>
<body>
  <table>
"""

FOOT = """\
  </table>
</body>
</html>
"""


@dataclass
class Talk:
    speakers: List[str]
    room: str
    title: str
    subtitle: str
    date: str
    apology: str


class TemplateEngine:
    def __init__(self):
        self.conversions = []

    def process_template(self, template, talk, output):
        """Generate an SVG from a template and a talk to output, as sreview does

        Only the templated SVG is written at this stage, the SVG->PNG
        conversion is batched for later.
        """
        if len(talk.speakers) > 2:
            comma_names = ', '.join(talk.speakers[:-1])
            speakers = f'{comma_names} and {talk.speakers[-1]}'
        else:
            speakers = ' and '.join(talk.speakers)

        svg_path = output.with_suffix('.svg')
        with svg_path.open('w') as f:
            f.write(template
                .replace('@SPEAKERS@', xml_escape(speakers))
                .replace('@ROOM@', xml_escape(talk.room))
                .replace('@TITLE@', xml_escape(talk.title))
                .replace('@SUBTITLE@', xml_escape(talk.subtitle))
                .replace('@DATE@', xml_escape(str(talk.date)))
                .replace('@APOLOGY@', xml_escape(talk.apology))
            )

        self.conversions.append((svg_path, output))

    def convert_pngs(self):
        """Batch convert all the SVGs to PNG"""
        p = Popen(('inkscape', '--shell'), stdin=PIPE, stdout=PIPE)
        commands = []
        for svg, png in self.conversions:
            commands.append(
                f'file-open:{svg}; export-type:png; export-filename:{png}; '
                 'export-do')
        commands.append('quit')
        stdout, stderr = p.communicate('\n'.join(commands).encode('utf-8'))
        if p.wait() != 0:
            print(f'stdout: {stdout}\nstderr: {stderr}')
        for svg, png in self.conversions:
            svg.unlink()


def preview(talks, templates, output):
    """Write a simple HTML page previewing all templates for all talks"""
    te = TemplateEngine()
    with output.joinpath('index.html').open('w') as html:
        html.write(HEAD)
        for i, talk in enumerate(talks):
            html.write('    <tr>\n')
            for j, template in enumerate(templates):
                rendered = output.joinpath(f'{i}-{j}.png')
                te.process_template(template, talk, rendered)
                html.write('      <td>\n')
                html.write(f'        <img src="{rendered.name}">\n')
                html.write('      </td>\n')
            html.write('    </tr>\n')
        html.write(FOOT)
    te.convert_pngs()


def main():
    p = ArgumentParser()
    p.add_argument('--template', '-t', action='append', type=open,
                   help='Process an SVG template. '
                        'Repeat for multiple templates.')
    p.add_argument('--limit', '-l', type=int, metavar='N',
                   help='Stop after the first N talks.')
    p.add_argument('--output', default='output',
                   help='Directory to write output to. '
                        'All existing content will be deleted.')
    p.add_argument('--talks', default='talks.yml', type=open,
                   help='YAML file containing talks.')
    args = p.parse_args()

    talks = safe_load(args.talks)
    args.talks.close()
    if args.limit:
        talks = talks[:args.limit]
    talks = [Talk(**talk) for talk in talks]

    output = Path(args.output)
    if output.exists():
        rmtree(output)
    output.mkdir()

    templates = []
    for template in args.template:
        templates.append(template.read())
        template.close()

    preview(talks, templates, output)


if __name__ == '__main__':
    main()
