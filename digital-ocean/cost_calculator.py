#!/usr/bin/env python3
from datetime import datetime
from pytz import utc

upgrade = datetime(2020, 8, 18, tzinfo=utc)
end = datetime(2020, 8, 31, tzinfo=utc)
instances = {
    'ams1': [{
        'quota': 1,
        'price': 0.007,
        'start': datetime(2020, 7, 11, 18, tzinfo=utc),
        'stop': upgrade,
    }, {
        'quota': 4,
        'price': 0.03,
        'start': upgrade,
        'stop': end,
    }],
    'blr1': [{
        'quota': 1,
        'price': 0.007,
        'start': datetime(2020, 8, 15, 20, tzinfo=utc),
        'stop': upgrade,
    }, {
        'quota': 4,
        'price': 0.03,
        'start': upgrade,
        'stop': end,
    }],
    'sfo1': [{
        'quota': 1,
        'price': 0.007,
        'start': datetime(2020, 8, 15, 20, tzinfo=utc),
        'stop': upgrade,
    }, {
        'quota': 4,
        'price': 0.03,
        'start': upgrade,
        'stop': end,
    }],
    'sgp1': [{
        'quota': 1,
        'price': 0.007,
        'start': datetime(2020, 8, 15, 20, tzinfo=utc),
        'stop': upgrade,
    }, {
        'quota': 4,
        'price': 0.03,
        'start': upgrade,
        'stop': end,
    }],
}

quota = {}
cost = {}
for instance, periods in instances.items():
    cost[instance] = []
    quota[instance] = []
    for period in periods:
        time_alive = period['stop'] - period['start']
        hours = time_alive.days * 24 + time_alive.seconds // 60 // 60
        cost[instance].append(hours * period['price'])
        quota[instance].append(hours * period['quota'])
    print('{}: {} GiB @ ${}'.format(
        instance, sum(quota[instance]), sum(cost[instance])))

print('Total GiB: ',
      sum(sum(instance_quota) for instance_quota in quota.values()))
print('Total $: ',
      sum(sum(instance_cost) for instance_cost in cost.values()))
