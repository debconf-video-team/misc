#!/usr/bin/python3

from collections import defaultdict
from functools import partial
from ipaddress import ip_address, IPv4Address, IPv6Network
from pathlib import Path
import gzip
import re

import geoip2.database
import geoip2.errors


SERVERS = ['ams1', 'blr1', 'gva1', 'sfo1', 'sfo2', 'sgp1']

HLS_RE = re.compile(
    r'^(?P<ip>\S+) - - \[\S{20} [+-]\d{4}\] "GET '
    r'/live/(?P<stream>\S+?)-\d+.ts HTTP/\d\.\d" 200 (?P<bytes>\d+) ".*')

RTMP_RE = re.compile(
    r'^(?P<ip>\S+) \[\S{20} [+-]\d{4}\] PLAY "front" "(?P<stream>\S+?)" '
    r'"" - \d+ (?P<bytes>\d+) "" ".*" '
    r'\(((?P<hours>\d+)h )?((?P<mins>\d+)m )?(?P<secs>\d+)s\)$')


def normalize_ip(ip):
    """Normalize IPv4 addresses, and strip the host-part of IPv6 addresses, to
    account for privacy extensions, and consistency with IPv4.
    Generally an IPv6 /64 equates to an IPv4 /32 + NAT.
    """
    ip = ip_address(ip)
    if isinstance(ip, IPv4Address):
        return ip.compressed
    if ip.ipv4_mapped:
        return ip.ipv4_mapped.compressed
    return IPv6Network(str(ip) + '/64', strict=False).compressed


def parse(line):
    """Parse nginx-rtmp logs, counting HLS and RTMP plays"""
    m = HLS_RE.match(line)
    if m:
        return {
            'ip': normalize_ip(m.group('ip')),
            'stream': m.group('stream'),
            'bytes': m.group('bytes'),
            'duration': 5,
        }

    m = RTMP_RE.match(line)
    if m:
        hours = int(m.group('hours') or 0)
        mins = int(m.group('mins') or 0)
        secs = int(m.group('secs') or 0)
        duration = (hours * 60 + mins) * 60 + secs
        return {
            'ip': normalize_ip(m.group('ip')),
            'stream': m.group('stream'),
            'bytes': m.group('bytes'),
            'duration': duration,
        }


def iter_plays(servers):
    """Iterate through all the plays in all the log files"""
    for server in servers:
        logs = Path(server)
        for log in logs.glob('*access.log*'):
            opener = log.open
            if log.suffix == '.gz':
                opener = partial(gzip.open, str(log))
            with opener('rt', encoding='utf8') as f:
                for line in f:
                    m = parse(line)
                    if m:
                        yield m


def main():
    by_country = defaultdict(int)
    by_ip = defaultdict(int)
    countries = {}
    country_names = {}
    reader = geoip2.database.Reader('GeoLite2-City.mmdb')
    for play in iter_plays(SERVERS):
        ip = play['ip']
        duration = play['duration']
        country = countries.get(ip)
        if not country:
            try:
                geo_info = reader.city(ip.split('/', 1)[0])
                country = geo_info.country.iso_code
                countries[ip] = country
                country_names[country] = geo_info.country.names.get('en')
            except geoip2.errors.AddressNotFoundError:
                pass
        by_country[country] += duration
        by_ip[ip] += duration

    # Play >= 20 mins to be considered
    sufficient = [(duration, ip) for ip, duration in by_ip.items()
                  if duration > 20 * 60]
    sufficient.sort()

    with open('by_ip.txt', 'w') as f:
        for duration, ip in sufficient:
            mins = duration // 60
            hrs = mins // 60
            mins %= 60
            country = countries.get(ip)
            f.write(f'{hrs}:{mins} {ip} {country}\n')

    # Play >= 20 mins to be considered
    sufficient = [(duration, country)
                  for country, duration in by_country.items()
                  if duration > 20 * 60]
    sufficient.sort()

    with open('by_country.txt', 'w') as f:
        for duration, country in sufficient:
            mins = duration // 60
            hrs = mins // 60
            mins %= 60
            country_name = country_names[country]
            f.write(f'{hrs}:{mins} {country} {country_name}\n')

    print('IPv4:', sum(v for k, v in by_ip.items() if ':' not in k) / 60 / 60)
    print('IPv6:', sum(v for k, v in by_ip.items() if ':' in k) / 60 / 60)
    print('Total Hours watched:', sum(by_ip.values()) / 60 / 60)


if __name__ == '__main__':
    main()
