#!/bin/sh

set -eufx
for frontend in ams1 blr1 sfo1 sgp1 onsite; do
	rsync -avzP root@$frontend.live.debconf.org:/var/log/nginx/ $frontend/
done
