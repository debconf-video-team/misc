#!/usr/bin/env python3

from argparse import ArgumentParser
from csv import DictWriter
from xml.etree import ElementTree
import re

import requests


def main():
    p = ArgumentParser()
    p.add_argument('pentabarf_url', help='Pentabarf XML Schedule URL')
    p.add_argument('video_url', help='Published video URL base')
    args = p.parse_args()

    with requests.get(args.pentabarf_url) as r:
        tree = ElementTree.fromstring(r.content)

    talks = []
    for room in tree.findall('.//room'):
        for event in room.findall('event'):
            url = event.findtext('conf_url')
            m = re.match(r'^/talks/((\d+)-.*)/$', url)
            slug = m.group(1)
            talks.append({
                'id': m.group(2),
                'name': event.findtext('title'),
                'hq': f'{args.video_url}/{slug}.webm',
                'lq': f'{args.video_url}/{slug}.lq.webm',
            })
    with open('sheet.csv', 'w') as f:
        writer = DictWriter(f, ('id', 'name', 'hq', 'lq'))
        writer.writeheader()
        for talk in talks:
            writer.writerow(talk)


if __name__ == '__main__':
    main()
