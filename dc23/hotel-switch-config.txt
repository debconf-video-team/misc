config-file-header
sw-hotel
v1.3.0.62 / R750_NIK_1_3_647_260
CLI v1.0
set system mode switch 

file SSD indicator encrypted
@
ssd-control-start
ssd config
ssd file passphrase control unrestricted
no ssd file integrity control
ssd-control-end cb0a3fdb1f3a1af4e4430033719968c0
!
vlan database
vlan 5,10,15
exit
voice vlan oui-table add 0001e3 Siemens_AG_phone________
voice vlan oui-table add 00036b Cisco_phone_____________
voice vlan oui-table add 00096e Avaya___________________
voice vlan oui-table add 000fe2 H3C_Aolynk______________
voice vlan oui-table add 0060b9 Philips_and_NEC_AG_phone
voice vlan oui-table add 00d01e Pingtel_phone___________
voice vlan oui-table add 00e075 Polycom/Veritel_phone___
voice vlan oui-table add 00e0bb 3Com_phone______________
ip dhcp snooping information option allowed-untrusted
hostname sw-hotel
username cisco password encrypted 281f465401ca51839ded7d8ae2e3e839c1bacc75 privilege 15
ip ssh server
ip http timeout-policy 0
ip telnet server
!
interface vlan 1
 ip address 192.168.1.253 255.255.255.0
 no ip address dhcp
!
interface vlan 5
 name Wifi
!
interface vlan 10
 name Uplink
!
interface vlan 15
 name Video
!
interface gigabitethernet1
 ip dhcp snooping trust
 switchport mode access
 switchport access vlan 5
 lldp optional-tlv port-desc sys-name sys-desc sys-cap
!
interface gigabitethernet2
 switchport mode access
 switchport access vlan 5
 lldp optional-tlv port-desc sys-name sys-desc sys-cap
!
interface gigabitethernet3
 switchport mode access
 switchport access vlan 5
 lldp optional-tlv port-desc sys-name sys-desc sys-cap
!
interface gigabitethernet4
 switchport trunk allowed vlan add 5,15
 lldp optional-tlv port-desc sys-name sys-desc sys-cap
 switchport default-vlan tagged
!
interface gigabitethernet5
 switchport trunk allowed vlan add 5,15
 lldp optional-tlv port-desc sys-name sys-desc sys-cap
 switchport default-vlan tagged
!
interface gigabitethernet6
 switchport mode access
 lldp optional-tlv port-desc sys-name sys-desc sys-cap
!
interface gigabitethernet7
 ip dhcp snooping trust
 switchport mode access
 switchport access vlan 15
 lldp optional-tlv port-desc sys-name sys-desc sys-cap
!
interface gigabitethernet8
 switchport mode access
 switchport access vlan 15
 lldp optional-tlv port-desc sys-name sys-desc sys-cap
!
interface gigabitethernet9
 switchport mode access
 switchport access vlan 15
 lldp optional-tlv port-desc sys-name sys-desc sys-cap
!
interface gigabitethernet10
 switchport mode access
 switchport access vlan 15
 lldp optional-tlv port-desc sys-name sys-desc sys-cap
!
interface gigabitethernet11
 switchport mode access
 switchport access vlan 15
 lldp optional-tlv port-desc sys-name sys-desc sys-cap
!
interface gigabitethernet12
 switchport mode access
 switchport access vlan 15
 lldp optional-tlv port-desc sys-name sys-desc sys-cap
!
interface gigabitethernet13
 switchport mode access
 switchport access vlan 5
 lldp optional-tlv port-desc sys-name sys-desc sys-cap
!
interface gigabitethernet14
 switchport mode access
 switchport access vlan 5
 lldp optional-tlv port-desc sys-name sys-desc sys-cap
!
interface gigabitethernet15
 switchport mode access
 switchport access vlan 5
 lldp optional-tlv port-desc sys-name sys-desc sys-cap
!
interface gigabitethernet16
 switchport trunk allowed vlan add 5,15
 lldp optional-tlv port-desc sys-name sys-desc sys-cap
 switchport default-vlan tagged
!
interface gigabitethernet17
 switchport trunk allowed vlan add 5,15
 lldp optional-tlv port-desc sys-name sys-desc sys-cap
 switchport default-vlan tagged
!
interface gigabitethernet18
 switchport mode access
 lldp optional-tlv port-desc sys-name sys-desc sys-cap
!
interface gigabitethernet19
 switchport mode access
 switchport access vlan 15
 lldp optional-tlv port-desc sys-name sys-desc sys-cap
!
interface gigabitethernet20
 switchport mode access
 switchport access vlan 15
 lldp optional-tlv port-desc sys-name sys-desc sys-cap
!
interface gigabitethernet21
 switchport mode access
 switchport access vlan 15
 lldp optional-tlv port-desc sys-name sys-desc sys-cap
!
interface gigabitethernet22
 switchport mode access
 switchport access vlan 15
 lldp optional-tlv port-desc sys-name sys-desc sys-cap
!
interface gigabitethernet23
 switchport mode access
 switchport access vlan 15
 lldp optional-tlv port-desc sys-name sys-desc sys-cap
!
interface gigabitethernet24
 switchport mode access
 switchport access vlan 15
 lldp optional-tlv port-desc sys-name sys-desc sys-cap
!
interface gigabitethernet25
 switchport mode access
 switchport access vlan 10
 lldp optional-tlv port-desc sys-name sys-desc sys-cap
!
interface gigabitethernet26
 switchport mode access
 switchport access vlan 10
 lldp optional-tlv port-desc sys-name sys-desc sys-cap
!
interface gigabitethernet27
 switchport mode access
 switchport access vlan 5
 lldp optional-tlv port-desc sys-name sys-desc sys-cap
!
interface gigabitethernet28
 switchport trunk allowed vlan add 5,10,15
 lldp optional-tlv port-desc sys-name sys-desc sys-cap
 switchport default-vlan tagged
!
exit
macro auto disabled
macro auto processing type host enabled
macro auto processing type ip_phone_desktop disabled
macro auto processing type ap disabled
